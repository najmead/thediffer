##The Difference Engine

The Difference Engine is a small bash script written to query your Kodi media player for new movies and tv shows and then email an update.  It should also work with Kodi derivatives such as OpenELEC, as long as they support V6 of Kodi's JSON-RPC API.  The script itself has been written on Open Media Vault (Debian), but should work on any POSIX-compliant system with bash, assuming the pre-requisites have been installed (see below).

###Prerequisites

The Difference Engine is designed to run in a bash shell.  It requires sqlite3 in order to store data, and jq in order to parse the JSON results that Kodi returns.  If you want to take advantage of the automated emailing function, you'll also need to install mpack, although the script should work without it.  These pieces of software should be available in the repository of whatever distribution you are using.

###Getting Started

Before getting started, you'll need to edit the script to add the address of your media player.  You'll probably also need to make the script executable (chmod +x differ).

When you run the script, it'll query Kodi and get a list of TV shows and movies, with the associated date they were added to the Kodi library, and proceed to add them to a local sqlite database called data.db.  Any entries that are identified as "new" will be piped into an html document called update.html.  If you specify an email address, the results will be emailed to this address.

###Options

When you run the script, the followig parameters are available;

-d <number of days>

The script will scan for content added in the past <number of days>.  If you don't specify -d, the script will default to 1900-01-01 -- in other words, it will try and scan your entire Kodi library.

-m <email address>

The user specified by -m will be emailed with any updates identified.  If you don't use the option -m, the script will prepare the update html file, but will not email it.  This is useful if you want to use a different email mechanism.

-s <server>

The address of your Kodi media player.  Obviously, this is mandatory :)

-p <port>

The port that your Kodi system is listening on.  If no port is specified, it will be assumed to be port 80.

-u username:password

The username and password, if the Kodi HTTP server requires one.

-U

If the option -u is specified, the script will send an update request to Kodi, and wait until the library scanning has finished before proceeding.  Depending on the size of your library, this may add several minutes to the time it takes to run the script.

###First Run

On first run, it's recommended you run without the -d option specified.  This will basically create an entry for ALL the content in your library.  If you do this, later on if you accidentally hose your Kodi library and have to re-add all your content, re-running differ WILL NOT send out an update with all your content suddenly identified as "new".

Since the first run will be scanning the entire library and attempting to parse quite a lot of JSON content, it can be quite slow.  To help speed this up, episodes data (which can be particularly large) is broken into smaller batches.  The default batchsize is 1000 records, but on slower systems (particularly those with slower I/O) it might be worth making this smaller.  You can change the batchsize by editing the variable BATCHSIZE.

On subsequent runs, you should specify the -d option.  This will limit the amount of data that is queried from Kodi, and therefore, will be much faster.  However, if you completely clean out your Kodi library and re-add, all the content will be identified as "new" and so the next scan will need to at least try and parse the content to see if it really is new.  Since differ checks it's own database, though, only genuinely new content will be identified.

###Caveat

Since the script is designed to work even if you have to rebuild your Kodi library from scratch, it doesn't rely on Kodi's own ids for identifying content (since these will be regenerated).  Instead, the unique key for movies is the IMDB id, and the unique key for television is the TVDB id.  Consequently, if you add a new movie to your library with the same IMDB id as an existing movie (eg, adding the directors cut), it won't be identified as new in the updates.

###Warning

The Difference Engine has been designed entirely based on my own system and my own library of media.  It might work on yours.  It might not.  It's probably not going to do any damage, but I can't guarantee that it won't break some stuff or at the very least, throw out some angry error messages.  I'm a novice bash programmer at best.



